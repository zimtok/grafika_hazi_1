public abstract class DrawMode
{
  protected DrawArea drawArea;
  protected boolean drawing = false;
  
  protected void addShape(Shape shape)
  {
    drawArea.addShape(shape);
  }
  
  public abstract void handleClick(int x, int y);
  
  public void handleDrag(int x, int y) {return;}
  
  public void handleRelease(){return;}
  
  public void handleKeyPress(char key, int keyCode){return;}
  
  public abstract void shapeInProgress(int x, int y);
  
  public void cancel()
  {
      this.drawing = false;
  }
 
}