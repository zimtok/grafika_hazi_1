public class Line extends Shape
{
    private PVector endpoint;



    public Line(PVector offset, PVector endpoint)
    {
        this.endpoint = endpoint;
        this.offset = offset;
    }

    @Override
    public void drawShape()
    {
        line(0, 0, this.endpoint.x, this.endpoint.y);
    }

    @Override
    public PVector getCenter()
    {
        PVector center = new PVector();
        center.x = this.endpoint.x / 2.0;
        center.y = this.endpoint.y / 2.0;
        return center;
    }

    @Override
    public boolean contains(PVector point)
    {
       float hitbox = 0.5;
        //Check if the sum of the distances between the given point and the two
        //endpoints is equal (or close) to the distance between the two endpoints  
       PVector translatedEndpoint = offset.copy().add(endpoint);
       float d1 = offset.dist(translatedEndpoint);
       float d2 = point.dist(offset);
       float d3 = point.dist(translatedEndpoint);
       
       return abs(d2 + d3 - d1) < hitbox; 
    }
    
    @Override
    public Shape duplicate()
    {
        Line ret = new Line(offset.copy().add(duplicateOffset), endpoint);
        ret.setColor(col);
        ret.setRotation(rotation);
        return ret;
    }
    
    @Override
    public JSONObject JSONConvert()
    {
         JSONObject ret = super.JSONConvert();
         ret.setString("type","line");
         ret.setJSONObject("endpoint",vectorToJSON(endpoint));
         return ret;
    }
}