public abstract class Shape implements java.io.Serializable
{
    protected final color selectionColor = color(66, 200, 244);
    protected float rotation = 0;
    protected color col;
    protected PVector offset;
    protected PVector scaleFactor = new PVector(1, 1);
    protected final PVector duplicateOffset = new PVector(-30, -30);
    protected boolean selected = false;

    public void update()
    {
        pushStyle();
        pushMatrix();
        stroke(col);
        PVector c = getCenter();
        translate(offset.x + c.x, offset.y + c.y);
        rotate(rotation % PI * 2);
        scale(scaleFactor.x, scaleFactor.y);
        translate(-c.x, -c.y);

        if (selected) {
            stroke(selectionColor);
            strokeWeight(2);
        }
        drawShape();
        popMatrix();
        popStyle();
    }

    public abstract void drawShape();

    public abstract PVector getCenter();

    public PVector getCenterWithOffset()
    {
        return this.getCenter().add(this.offset);
    }

    public abstract boolean contains(PVector point);

    public void setRotation(float rotation)
    {
        this.rotation += rotation;
        this.rotation = this.rotation % TWO_PI;
    }

    public void select()
    {
        selected = true;
    }

    public void deselect()
    {
        selected = false;
    }

    public void drag(PVector direction)
    {
        offset.add(direction);
    }

    public abstract Shape duplicate();

    public color getColor()
    {
        return this.col;
    }

    public void setColor(color col)
    {
        this.col = col;
    }
    
    public void setScale(PVector scale)
    {
        this.scaleFactor = scale;
    }
    
    public void setFill(color c){return;}
    
    public JSONObject JSONConvert()
    {
        JSONObject ret = new JSONObject();
        ret.setJSONObject("offset",vectorToJSON(offset));
        ret.setJSONObject("scaleFactor",vectorToJSON(scaleFactor));
        ret.setFloat("rotation", rotation);
        JSONObject c = colorToJSON(col);
        ret.setJSONObject("color",c);
        return ret;
    }
    
    protected JSONObject vectorToJSON(PVector vec)
    {
        JSONObject ret = new JSONObject();
        ret.setFloat("x",vec.x);
        ret.setFloat("y",vec.y);
        return ret;
    }
    
    protected JSONObject colorToJSON(color c)
    {
        JSONObject ret = new JSONObject();
        ret.setFloat("r",red(c));
        ret.setFloat("g", green(c));
        ret.setFloat("b",blue(c));
        return ret;
    }
    
}