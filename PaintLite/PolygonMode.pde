public class PolygonMode extends DrawMode
{
    protected PVector start;
    protected ArrayList<PVector> vertices, normalizedVertices;
    protected final int DistToFinish = 5;
    public PolygonMode(DrawArea drawArea)
    {
        this.drawArea = drawArea;
        this.drawing = false;
        this.vertices = new ArrayList<PVector>();
        this.normalizedVertices = new ArrayList<PVector>();
        this.drawArea.setSelection(null);
    }
    
    @Override
    public void handleClick(int x, int y)
    {
        PVector clickedPoint = new PVector(x, y);
        if(!drawing){
            drawing = true;
            start = clickedPoint;
            vertices.add(new PVector(x,y));
            normalizedVertices.add(new PVector(0,0));
        }else{
            if(start.dist(clickedPoint) < DistToFinish){
                drawing = false;
                createShape();
                this.vertices = new ArrayList<PVector>();
                this.normalizedVertices = new ArrayList<PVector>();
            }else{
                vertices.add(clickedPoint);
                normalizedVertices.add(clickedPoint.copy().sub(start));
            }
        }
            
        
    }
    
    @Override
    public void shapeInProgress(int x, int y)
    {
        if(drawing){
            ellipse(start.x, start.y, DistToFinish, DistToFinish); 
            beginShape();
                vertex(start.x, start.y);
                for(PVector v : vertices){
                    vertex(v.x, v.y);
                }
                vertex(x,y);
            endShape();
        }
    }
    
    protected void createShape(){
        this.drawArea.addShape(new Polygon(start,normalizedVertices));
    }
    
}