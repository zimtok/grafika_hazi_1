public class Ellipse extends Shape
{
    private float radiusX;
    private float radiusY;
    private color fillColor;

    public Ellipse(PVector offset, float x, float y)
    {
        this.offset = offset;
        this.radiusX = x;
        this.radiusY = y;
    }

    @Override
        public void drawShape()
    {
        fill(fillColor);
        ellipse(0, 0, radiusX* 2, radiusY * 2);
    }

    @Override
    public PVector getCenter()
    {
        return new PVector(0,0);
    }

    @Override
    public boolean contains(PVector point)
    {
        return (sq((point.x - offset.x)/radiusX) + sq((point.y - offset.y)/radiusY)) <= 1.0;
    }
    
    @Override
    public Shape duplicate()
    {
        Ellipse ret = new Ellipse(offset.copy().add(duplicateOffset), radiusX, radiusY);
        ret.setColor(col);
        ret.setFill(fillColor);
        ret.setRotation(rotation);
        return ret;
    }
    

    
    public color getFillColor()
    {
        return this.fillColor;
    }
    
    @Override
    public void setFill(color fillColor)
    {
        this.fillColor = fillColor;
    }
    
    @Override
    public JSONObject JSONConvert()
    {
        JSONObject ret = super.JSONConvert();
        ret.setString("type","ellipse");
        ret.setFloat("radiusX",radiusX);
        ret.setFloat("radiusY",radiusY);
        JSONObject fc = colorToJSON(fillColor);
        ret.setJSONObject("fillColor", fc);
        return ret;
    }
}