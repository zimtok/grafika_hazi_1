public class ClosedCurve extends Polygon
{
    public ClosedCurve(PVector offset, ArrayList<PVector> vertices)
    {
        super(offset, vertices);
    }
    
    @Override
    public void drawShape()
    {
        int length = this.vertices.size();
         for(int i = 0; i < length; i++){
             PVector ctrl1 = vertices.get(i);
             PVector p1 = vertices.get((i + 1) % length);
             PVector p2 = vertices.get((i + 2) % length);
             PVector ctrl2 = vertices.get((i + 3) % length);
             curve(ctrl1.x, ctrl1.y, p1.x, p1.y, p2.x, p2.y, ctrl2.x, ctrl2.y);
         }
    }
    
    @Override
    public Shape duplicate()
    {
        ClosedCurve ret = new ClosedCurve(offset.copy().add(duplicateOffset), (ArrayList<PVector>)vertices.clone());
        ret.setColor(col);
        ret.setRotation(rotation);
        return ret;
    }
    
    @Override
    public JSONObject JSONConvert()
    {
        JSONObject ret = super.JSONConvert();
        ret.setString("type","closedCurve");
        return ret;
    }

}