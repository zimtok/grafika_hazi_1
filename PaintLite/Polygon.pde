

public class Polygon extends Shape
{
    protected ArrayList<PVector> vertices;
    
    public Polygon(PVector offset, ArrayList<PVector> vertices)
    {
        this.offset = offset;
        this.vertices = vertices;
    }
    
    @Override
    public void drawShape()
    {
        beginShape();
            for(PVector v : vertices){
                vertex(v.x, v.y);
            }
        endShape(CLOSE);
    }
    
    @Override
    public PVector getCenter()
    {
        PVector sum = new PVector(0,0);
        for(PVector v : vertices){
            sum.add(v);
        }
        int vertexCount = vertices.size();
        return sum.div(vertexCount);
    }
    
    @Override
    public boolean contains(PVector point)
    {
        //This method checks wether a line segment between the given point
        //and an arbitrary point outside the canvas (-100, -100)
        //intersects the sides of the polygon an odd number of times
        int intersectCount = 0;
        int length = vertices.size();
        PVector basePoint = new PVector(-100, -100);
        for( int i = 0; i < length; i++){
            PVector p1 = vertices.get(i).copy().add(offset);
            PVector p2 = vertices.get((i + 1) %length).copy().add(offset);
            if(intersect(point, basePoint, p1, p2)){
                intersectCount++;
            }
        }
        return (intersectCount % 2) == 1;
        
    }
    
    protected boolean counterCW(PVector a, PVector b, PVector c)
    {
        //Check if 3 points are in counter-clockwise orientation
        return (c.y-a.y)*(b.x-a.x) > (b.y-a.y)*(c.x-a.x);
    }
    
    protected boolean intersect(PVector p1, PVector p2, PVector q1, PVector q2)
    {   
        //If p1p2 intersects q1q2, then p1p2q1 and p1p2q2 will have different orientations.
        return counterCW(p1, q1, q2) != counterCW (p2, q1, q2) && counterCW(p1, p2, q1) != counterCW(p1, p2, q2);
    }
    
    @Override
    public Shape duplicate()
    {
        Polygon ret = new Polygon(offset.copy().add(duplicateOffset), (ArrayList<PVector>)vertices.clone());
        ret.setColor(col);
        ret.setRotation(rotation);
        return ret;
    }
    
    @Override
    public JSONObject JSONConvert()
    {
        JSONObject ret = super.JSONConvert();
        ret.setString("type","polygon");
        JSONArray vArray = new JSONArray();
        for(int i = 0; i < vertices.size(); i++){
            PVector v = vertices.get(i);
            JSONObject vObj = vectorToJSON(v);
            vArray.setJSONObject(i,vObj);
        }
        ret.setJSONArray("vertices",vArray);
        return ret;
    }
}