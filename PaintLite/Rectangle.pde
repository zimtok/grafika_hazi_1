public class Rectangle extends Shape
{
    private PVector dimension;
    private color fillColor;
    public Rectangle(PVector offset, PVector dimension)
    {
        this.offset = offset;
        this.dimension = dimension;
    }
    
    @Override
    public void drawShape()
    {
        fill(fillColor);
        rect(0, 0, dimension.x, dimension.y);
    }
    
    @Override
    public PVector getCenter()
    {
        return new PVector(dimension.x / 2, dimension.y / 2);
    }
    
    
    @Override
    public boolean contains(PVector point)
    {
        boolean vertical, horizontal;
        
        if(dimension.x > 0){
            horizontal = point.x >= offset.x && point.x <= offset.x + dimension.x;
        }else{
            horizontal = point.x <= offset.x && point.x >= offset.x + dimension.x;
        }
        if(dimension.y > 0){
            vertical = point.y >= offset.y && point.y <= offset.y + dimension.y;
        }else{
            vertical = point.y <= offset.y && point.y >= offset.y + dimension.y;
        }
        return horizontal && vertical; 
    }
    
    @Override
    public Shape duplicate()
    {
        Rectangle ret = new Rectangle(offset.copy().add(duplicateOffset), dimension);
        ret.setColor(col);
        ret.setFill(fillColor);
        ret.setRotation(rotation);
        return ret;
    }
        
    public color getFillColor()
    {
        return this.fillColor;
    }
    
    @Override
    public void setFill(color fillColor)
    {
        this.fillColor = fillColor;
    }
    
    @Override
    public JSONObject JSONConvert()
    {
        JSONObject ret = super.JSONConvert();
        ret.setString("type","rectangle");
        ret.setJSONObject("dimension",vectorToJSON(dimension));
        JSONObject fc = colorToJSON(fillColor);
        ret.setJSONObject("fillColor", fc);
        return ret;
    }
}