public class FillPicker extends ColorPicker
{
    public FillPicker(IntList colors, int posX, int posY, int width, int height)
    {
        this.colors = colors;
        this.posX = posX;
        this.posY = posY;
        this.width = width;
        this.height = height;
    }
    
    @Override
    public void drawContents(color currentColor, int x, int y, int cellWidth, int cellHeight)
    {
        fill(currentColor);
        ellipse(x + cellWidth / 2, y + cellHeight / 2, cellWidth * 0.5, cellHeight * 0.8);
    }
    
    @Override
    protected void updateCanvas(color c)
    {
        canvas.setFillColor(c);
    }
}