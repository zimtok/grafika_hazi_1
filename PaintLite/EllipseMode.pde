public class EllipseMode extends DrawMode
{
  private PVector center;
  
  public EllipseMode(DrawArea drawArea)
 {
   this.drawArea = drawArea;
   this.drawing = false;
   this.drawArea.setSelection(null);
 }
 
 @Override
 public void handleClick(int x, int y)
 {
   if(!drawing){
     drawing = true;
     center = new PVector(x,y);
   }else{
     drawing = false;
     PVector radiusVec = getRadius(x,y);
     Ellipse ellipseToAdd = new Ellipse(center, radiusVec.x, radiusVec.y);
     drawArea.addShape(ellipseToAdd);
   }
 }
 
 @Override
 public void shapeInProgress(int x, int y)
 {
  if(drawing){
    PVector radiusVec = getRadius(x,y);
    ellipse(center.x, center.y, radiusVec.x * 2, radiusVec.y * 2);
  }
 }
 
 private PVector getRadius(int x, int y)
 {
   PVector ret = new PVector(abs(center.x - x), abs(center.y - y));
   if(keyPressed && keyCode == CONTROL){
     float distance = ret.mag();
     return new PVector(distance, distance);
   }else{
     return ret;
   }
 }
 
}