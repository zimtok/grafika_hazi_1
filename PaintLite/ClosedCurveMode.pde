public class ClosedCurveMode extends PolygonMode
{
    public ClosedCurveMode(DrawArea drawArea){
        super(drawArea);
    }
    
    @Override
    protected void createShape(){
        this.drawArea.addShape(new ClosedCurve(start,normalizedVertices));
    }
}