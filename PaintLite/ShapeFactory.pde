public class ShapeFactory
{
    
    public Shape createShape(JSONObject data)
    {
        Shape result = null;
        String type = data.getString("type");
        switch(type){
            case "line":
                result = createLine(data);
                break;
            case "ellipse":
                result = createEllipse(data);
                break;
            case "rectangle":
                result = createRectangle(data);
                break;
            case "polygon":
                result = createPolygon(data);
                break;
            case "closedCurve":
                result = createClosedCurve(data);
                break;
        }
         return result;       
            
    }
    
    private Line createLine(JSONObject data)
    {
        Line line;
        PVector endpoint = unpackVector(data.getJSONObject("endpoint"));
        line = new Line(new PVector(0, 0), endpoint);
        setShapeData(line, data);
        return line;
    }
    
    private Ellipse createEllipse(JSONObject data)
    {
        Ellipse ellipse;
        float radiusX = data.getFloat("radiusX");
        float radiusY = data.getFloat("radiusY");
        ellipse = new Ellipse(new PVector(0, 0), radiusX, radiusY);
        setShapeData(ellipse, data);
        color c = unpackColor(data.getJSONObject("fillColor"));
        ellipse.setFill(c);
        return ellipse;
    }
    
    private Rectangle createRectangle(JSONObject data)
    {
        Rectangle rectangle;
        PVector dimension = unpackVector(data.getJSONObject("dimension"));
        rectangle = new Rectangle(new PVector(0, 0), dimension);
        setShapeData(rectangle, data);
        color c = unpackColor(data.getJSONObject("fillColor"));
        rectangle.setFill(c);
        return rectangle;
    }
    
    private Polygon createPolygon(JSONObject data)
    {
        Polygon polygon;
        ArrayList<PVector> vertices = getVertices(data.getJSONArray("vertices"));
        polygon = new Polygon(new PVector(0, 0), vertices);
        setShapeData(polygon, data);
        return polygon;
    }
    
    private ClosedCurve createClosedCurve(JSONObject data)
    {
        ClosedCurve closedCurve;
        ArrayList<PVector> vertices = getVertices(data.getJSONArray("vertices"));
        closedCurve = new ClosedCurve(new PVector(0, 0), vertices);
        setShapeData(closedCurve, data);
        return closedCurve;
    }
    
    private ArrayList<PVector>getVertices(JSONArray vArray)
    {
        ArrayList<PVector> vertices = new ArrayList<PVector>();
        for(int i = 0; i < vArray.size(); i++){
            JSONObject point = vArray.getJSONObject(i);
            vertices.add(unpackVector(point));
        }
        return vertices;
    }
    
    private void setShapeData(Shape shape, JSONObject data)
    {
        PVector offset = unpackVector(data.getJSONObject("offset"));
        shape.drag(offset);
        PVector scaleFactor = unpackVector(data.getJSONObject("scaleFactor"));
        shape.setScale(scaleFactor);
        color col = unpackColor(data.getJSONObject("color"));
        shape.setColor(col);
        float rotation = data.getFloat("rotation");
        shape.setRotation(rotation);
    }
    
    private PVector unpackVector(JSONObject json)
    {
        float x = json.getFloat("x");
        float y = json.getFloat("y");
        return new PVector(x, y);
    }
    
    private color unpackColor(JSONObject json)
    {
        float r = json.getFloat("r");
        float g = json.getFloat("g");
        float b = json.getFloat("b");
        return color(r, g, b);
    }
        
}