public class StrokePicker extends ColorPicker
{
    public StrokePicker(IntList colors, int posX, int posY, int width, int height)
    {
        this.colors = colors;
        this.posX = posX;
        this.posY = posY;
        this.width = width;
        this.height = height;
    }
    
    @Override
    public void drawContents(color currentColor, int x, int y, int cellWidth, int cellHeight)
    {
        stroke(currentColor);
        strokeWeight(3);
        line(x + 0.25 * cellWidth, y + 0.1 * cellHeight, x + 0.75 * cellWidth, y + 0.9 * cellHeight);
    }
    
    @Override
    protected void updateCanvas(color c)
    {
        this.canvas.setStrokeColor(c);
    }
}