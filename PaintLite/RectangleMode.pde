public class RectangleMode extends DrawMode
{
    private PVector start;
    private PVector dimension; 
    public RectangleMode(DrawArea drawArea)
    {
        this.drawArea = drawArea;
        this.drawing = false;
        this.drawArea.setSelection(null);
    }

    @Override
    public void handleClick(int x, int y)
    {
        if (!this.drawing) {
            this.drawing = true;
            this.start = new PVector(x, y);
        } else {
            this.drawing = false;
            this.dimension = new PVector(x - start.x, y - start.y);
            Rectangle lineToAdd = new Rectangle(this.start, this.dimension);
            this.drawArea.addShape(lineToAdd);
        }
    }

    @Override
        public void shapeInProgress(int x, int y)
    {
        if (drawing) {
            rect(start.x, start.y, x - start.x, y - start.y);
        }
    }
}