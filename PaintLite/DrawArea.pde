import java.io.*;

public class DrawArea
{
    private ArrayList<Shape> shapes;
    private int width;
    private int height;
    private int bgColor = 200;
    private DrawMode currentMode;
    private Shape selection;
    private color strokeColor;
    private color fillColor;
    
    public DrawArea(int width, int height) {
        this.shapes = new ArrayList<Shape>();
        this.width = width;
        this.height = height;
        this.currentMode = new SelectionMode(this);
    }
    public void update()
    {
        fill(bgColor);
        rect(0, 0, width, height);
        for (Shape shape : shapes) {
            shape.update(); //<>//
        }
        if(selection != null){
            selection.update();
        }
        currentMode.shapeInProgress(mouseX, mouseY);
    }

    public void handleClick(int x, int y) {
        if(mouseButton == LEFT){ 
            currentMode.handleClick(x, y);
        }else{
            currentMode.cancel();
        }
    }
    
    public void deleteShape(Shape shape)
    {
        shapes.remove(shape);
        selection = null;
    }

    public void moveToTop(Shape shape)
    {
        shapes.remove(shape);
        shapes.add(shape);
    }
    
    public void duplicateShape(Shape shape)
    {
        shapes.add(shape.duplicate());
    }
    public void handleKey(char key, int keyCode)
    {
        switch(key){
            case '0':
            currentMode = new SelectionMode(this);
            break;
            case '1':
            currentMode = new LineMode(this);
            break;
            case '2':
            currentMode = new EllipseMode(this);
            break;
            case '3':
            currentMode = new RectangleMode(this);
            break;
            case '4':
            currentMode = new PolygonMode(this);
            break;
            case '5':
            currentMode = new ClosedCurveMode(this);
            break;
            case 's':
            saveToFile();
            default:
            currentMode.handleKeyPress(key,keyCode);
            break;
        }
        
    }
    
    public void handleDrag(int x, int y)
    {
        currentMode.handleDrag(x,y);
    }
        
    public void handleRelease()
    {
         currentMode.handleRelease();   
    }

    public void addShape(Shape shape)
    {
        shape.setColor(strokeColor);
        shape.setFill(fillColor);
        this.shapes.add(shape);
    }
    
    public ArrayList<Shape> getShapes()
    {
        return this.shapes;
    }
    
    
    

    
    public void setSelection(Shape shape)
    {
        if(selection != null){
            selection.deselect();
        }
        selection = shape;
        if(selection != null){
            selection.select();
        }
    }
    
    public Shape getSelection()
    {
        return selection;
    }
    
    public color getStrokeColor()
    {
        return this.strokeColor;
    }
    
    public void setStrokeColor(color c)
    {
        this.strokeColor = c;
    }
    
    public color getFillColor()
    {
        return this.fillColor;
    }
    
    public void setFillColor(color c)
    {
        this.fillColor = c;
    }
    
    public void setShapes(ArrayList<Shape> shapes)
    {
        this.shapes = shapes;
    }
}