DrawArea canvas;
IntList colorsToUse;
ColorPicker strokeColor;
ColorPicker fillColor;


void setup()
{
    size(800, 600);
    colorsToUse = getColors();
    this.canvas = new DrawArea(750, 600);
    strokeColor = new StrokePicker(colorsToUse, 750, 0, 50, 275);
    strokeColor.bindTo(canvas);
    fillColor = new FillPicker(colorsToUse, 750, 275, 50, 275);
    fillColor.bindTo(canvas);
}

void draw()
{
    background(200);
    canvas.update();
    strokeColor.update();
    fillColor.update();
    pushStyle();
        stroke(canvas.getStrokeColor());
        fill(canvas.getFillColor());
        strokeWeight(3);
        rect(760, 560, 30, 30);
    popStyle();
}

void mouseClicked()
{
    if(strokeColor.contains(mouseX, mouseY)){
        strokeColor.handleClick(mouseX, mouseY);
    }else if(fillColor.contains(mouseX, mouseY)){
        fillColor.handleClick(mouseX, mouseY);
    }else{
        canvas.handleClick(mouseX, mouseY);
    }
    
}

void mouseDragged()
{
    canvas.handleDrag(mouseX, mouseY);
}

void mouseReleased()
{
    canvas.handleRelease();
}

void keyPressed()
{
    switch(key){
        case 's':
            saveToFile();
            break;
        case 'f':
            saveToJSON();
            break;
        case 'l':
            loadFromJSON();
            break;
        default:
            canvas.handleKey(key, keyCode);
    }
}

void saveToJSON()
{
    selectOutput("Mentés", "JSONExport");
}

void loadFromJSON()
{
    selectInput("Betöltés", "JSONImport");
}

void JSONImport(File JSONFile)
{
    ArrayList<Shape> newShapes = new ArrayList<Shape>();
    JSONArray shapeArray = loadJSONArray(JSONFile.getAbsolutePath());
    ShapeFactory factory = new ShapeFactory();
    for(int i = 0; i < shapeArray.size(); i++){
        Shape newShape = factory.createShape(shapeArray.getJSONObject(i));
        newShapes.add(newShape); //<>//
    }
    canvas.setShapes(newShapes);
}

void JSONExport(File JSONFile)
{
    JSONArray toExport = new JSONArray();
    ArrayList<Shape> shapes = canvas.getShapes();
    for(int i = 0; i < shapes.size(); i++){
        Shape shape = shapes.get(i);
        toExport.setJSONObject(i,shape.JSONConvert());
    }
    saveJSONArray(toExport, JSONFile.getAbsolutePath());
}

void saveToFile()
{
    selectOutput("Mentés képként", "saveAsImage");
}

public void saveAsImage(File imgFile)
{
    PImage canvasSave = get(0,0,750, 600);
    canvasSave.save(imgFile.getAbsolutePath());
}

IntList getColors()
{
   IntList ret = new IntList();
   ret.append(color(0));
   ret.append(color(255));
   ret.append(color(255, 0, 0));
   ret.append(color(0, 255, 0));
   ret.append(color(0, 0, 255));
   ret.append(color(255, 255, 0));
   ret.append(color(255, 0, 255));
   ret.append(color(0, 255, 255));
   ret.append(color(255, 125, 0));
   return ret;
    
}