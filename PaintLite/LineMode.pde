public class LineMode extends DrawMode
{
 
 private PVector start, end;
 
 
 public LineMode(DrawArea drawArea)
 {
   this.drawing = false;
   this.drawArea = drawArea;
   this.drawArea.setSelection(null);
 }
  
 @Override
 public void handleClick(int x, int y)
 {
   if(!this.drawing){
     this.drawing = true;
     this.start = new PVector(x, y);
   }else{
     this.drawing = false;
     this.end = new PVector(x - start.x, y - start.y);
     Line lineToAdd = new Line(this.start, this.end);
     this.drawArea.addShape(lineToAdd);
   }
   
 }
 
 @Override
 public void shapeInProgress(int x, int y)
 {
   if(drawing){
     line(start.x, start.y, x, y);
   }
 }
}