public class SelectionMode extends DrawMode
{
    private PVector oldMousePos;

    
    public SelectionMode(DrawArea drawArea)
    {
        this.drawArea = drawArea;
    }

    @Override
    public void handleClick(int x, int y)
    {
        PVector clickPos = new PVector(x, y);
        ArrayList<Shape> shapes = drawArea.getShapes();
        Shape toSelect = null;
        for(Shape shape : shapes){
            if(shape.contains(clickPos)){
               toSelect = shape;
            }
        }
        drawArea.setSelection(toSelect);
    }
    
    @Override
    public void handleDrag(int x, int y)
    {
        if(mouseButton == LEFT){
            dragShape(x, y);
        }else if(mouseButton == RIGHT){
            rotateShape(x,y);
        }else if(mouseButton == CENTER){
            scaleShape(x, y);
        }
    }
    
    @Override
    public void handleRelease()
    {
        oldMousePos = null;
    }
    
    @Override
    public void handleKeyPress(char key, int keyCode)
    {
        Shape selection = drawArea.getSelection();
        if(selection == null) return;
        if(key == CODED){
            switch(keyCode){
                case DELETE:
                     drawArea.deleteShape(selection);
                     break;
                default:
                    return;
            }
        }else{
            switch(key){
                 case DELETE:
                     drawArea.deleteShape(selection);
                     break;
                 case 'u':
                     drawArea.moveToTop(selection);
                     break;
                 case 'd':
                     drawArea.duplicateShape(selection);
                     break;
                 
            }
            
        }
        
    }
    
    private void dragShape(int x, int y)
    {
        Shape selection = drawArea.getSelection();
        if(selection == null){
            return;
        }
        if(oldMousePos != null){
            selection.drag(new PVector(x - oldMousePos.x, y - oldMousePos.y));
        }
        oldMousePos = new PVector(x, y);
    }
    
    private void rotateShape(int x, int y)
    {
        Shape selection = drawArea.getSelection();
        if(selection == null){
            return;
        }
        if(oldMousePos == null){
            oldMousePos = new PVector(x,y);
        }
        PVector center = selection.getCenterWithOffset();
        float oldAngle = atan2(oldMousePos.y - center.y, oldMousePos.x - center.x);
        float newAngle = atan2(y - center.y, x- center.x);
        selection.setRotation(newAngle - oldAngle);
        oldMousePos = new PVector(x,y);
    }
    
    private void scaleShape(int x, int y)
    {
        Shape selection = drawArea.getSelection();
        if(selection == null) return;
        if(oldMousePos == null) oldMousePos = new PVector(x,y);
        PVector center = selection.getCenterWithOffset();
        PVector originalDist = center.copy().sub(oldMousePos);
        PVector currentDist = center.copy().sub(x,y);
        PVector scale = new PVector(currentDist.x / originalDist.x, currentDist.y / originalDist.y);
        selection.setScale(scale);
    }

    @Override
        public void shapeInProgress(int x, int y)
    {
        return;
    }
}