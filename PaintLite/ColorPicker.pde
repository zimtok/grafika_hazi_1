public abstract class ColorPicker
{
    protected IntList colors;
    protected int posX, posY, width, height, highlighted;
    protected DrawArea canvas;
    
    public void update()
    {
        int colorCount = colors.size();
        int cellHeight = height / colorCount;
        for(int i = 0; i < colorCount; i++){
            pushStyle();
                int cellX = posX;
                int cellY = i * cellHeight + posY;
                rect(cellX, cellY, width, cellHeight);
                drawContents(colors.get(i), cellX, cellY, width, cellHeight);
            popStyle();
        }
        
        
    }
    
    public abstract void drawContents(color currentColor, int x, int y, int cellWidth, int cellHeight);
    
    public void bindTo(DrawArea canvas)
    {
        this.canvas = canvas;
    }
    
    public boolean contains(int x, int y)
    {
        return x > posX && x < posX + this.width && y > posY && y < posY + this.height;
    }
    
    public void highlight(int x, int y)
    {
        return;
    }
    
    public void handleClick(int x, int y)
    {
        //This method assumes that the coordinates are inside the ColorPicker (that should always be checked first)
        int colorCount = colors.size();
        int cellHeight = height / colorCount;
        int index = (y - posY) / cellHeight;
        updateCanvas(colors.get(index));
    }
    
    protected abstract void updateCanvas(color c);
}