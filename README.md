2D rajzolóprogram
=================

A programmal a következő alakzatokat lehet rajzolni:

* szakasz: egy kattintással a kezdő-, majd a kövezkező kattintással a végpontját jelöljük ki.
* ellipszis: egy kattintással a középpontot jelölhetjük ki, a következő kattintásra megjelenő ellipszis két tengelye az egér középponttól való (x- és y- tengely menti) távolságának felel meg. A Ctrl billentyű nyomva tartásával kör rajzolható.
* téglalap: két kattintással jelölhető ki a téglalap két szemközti csúcsa
* tetszőleges sokszög: Egymás után kattintásokkal helyzhetőek el a sokszög csúcsai (a befejezéshez kattintusunk az első csúcs körüli körbe)
* görbült alakzat: a rajzolás módja megegyezik a sokszögével, de a csúcsokra bezárt Catmull-Rom spline-it illeszt a program.

A rajzolási módok között az 1-5 billentyűkkel lehet váltani.

A 0 billenytű lenyomása után lehetséges kattintással kijelölni egty alakzatot (a kijelölt alakzat színe világoskék lesz). Egy kijelölt alakzatot lehetséges:
* mozgatni a bal egérgombot nyomva tartva
* forgatni a jobb egérgombot nyomva tartva
* nagyítani/kicsinyíteni a középső egérgombot nyomva tartva
* minden más alakzat fölé hozni az U billentyűvel
* törölni a Delete billenytyűvel
* megkettőzni a D billentyűvel

A képernyő jobb oldalán levő gombokkal állítható a körvonal és a kitöltés színe (kitöltési színe a téglalapoknak és az ellipsziseknek van). Az aktuális körvonal és kitöltési szín a jobb alsó sarokban levő négyzeten látható.

Az S billenytűvel a rajzterület képként elmenthető. Az aktuális alakzatok az F billentyűvel egy (JSON formátumú) fájlba menthetőek. Az L billentyűvel betölthető egy korábbi mentett rajz, és a szerkesztése folytathetó. 
 


